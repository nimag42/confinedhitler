import random
from datetime import datetime
from flask import Flask, request, render_template
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
socketio = SocketIO(app)

clients = []
roles = {}

@app.route("/")
def hello():
    return render_template('index.html')

@socketio.on('connected')
def io_handle_connected():
    print(clients)
    if(len(clients) < 2):
        print('Client connected : %s.' % request.sid)
        clients.append(request.sid)

        if(len(clients) == 2): # Start the game
            p, c = start_game()
            roles['president'] = p
            roles['chancellor'] = c
    else:
        emit('error', {'msg': 'There is too much clients. Retry later'})

@socketio.on('disconnect')
def io_hanlde_disconnect():
    print('Client disconnected : %s.' % request.sid)
    clients.remove(request.sid)
    emit("userlist", clients, broadcast=True)


def start_game():
    players = random.sample(clients, len(clients))
    president = players[0]
    chancellor = players[1]

    # Emit role
    emit('role', 'president', room=president)
    emit('role', 'chancellor', room=chancellor)

    return president, chancellor

@socketio.on('pick_laws')
def president_pick_laws():
    # Shuffle law
    laws = ['fascist' for x in range(11)] + ['liberal' for x in range(7)]
    random.shuffle(laws)

    print(clients)
    print(roles)
    # Emit laws to president
    emit('president_choose_laws', laws[:3], room=roles['president'])

@socketio.on('president_pass_law_to_chancellor')
def president_pass_law_to_chancellor(data):
    emit('president_pass_law_to_chancellor', data, room=roles['chancellor'])

@socketio.on('vote_law')
def chancellor_vote_law(data):
    emit('law_voted', data, broadcast=True)

def hello_to_random_client():
    if clients:
        k = random.choice(clients)
        print("Saying hello to %s" % k)
        emit('message', 'hello at %s' % datetime.now(), room=k)
    return 200

if __name__ == '__main__':
    socketio.run(app)
